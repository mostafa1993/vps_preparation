#!/bin/bash


red='\e[91m'
green='\e[92m'
yellow='\e[93m'
magenta='\e[95m'
cyan='\e[96m'
none='\e[0m'
_red() { echo -e ${red}$*${none}; }
_green() { echo -e ${green}$*${none}; }
_yellow() { echo -e ${yellow}$*${none}; }
_magenta() { echo -e ${magenta}$*${none}; }
_cyan() { echo -e ${cyan}$*${none}; }

[[ $(id -u) != 0 ]] && echo -e "You must be root.\n" && exit 1
cmd="apt-get"
sys_bit=$(uname -m)

case $sys_bit in
  'amd64' | x86_64)
    v2ray_bit="64"
    caddy_arch="amd64"
    ;;
  *aarch64* | *armv8*)
    v2ray_bit="arm64-v8a"
    caddy_arch="arm64"
    ;;
  *)
    echo -e "Only Ubuntu 16+ / Debian 8+ / CentOS 7+ systems are supported." && exit 1
    ;;
esac

if [[ $(command -v apt-get) || $(command -v yum) ]] && [[ $(command -v systemctl) ]]; then
  if [[ $(command -v yum) ]]; then
    cmd="yum"
  fi
else
  echo -e "Only Ubuntu 16+ / Debian 8+ / CentOS 7+ systems are supported." && exit 1
fi

uuid=$(cat /proc/sys/kernel/random/uuid)
old_id="e55c8d17-2cf3-b21a-bcf1-eeacb011ed79"
v2ray_server_config="/etc/v2ray/config.json"
v2ray_client_config="/etc/v2ray/233blog_v2ray_config.json"
backup="/etc/v2ray/233blog_v2ray_backup.conf"
_v2ray_sh="/usr/local/sbin/v2ray"
systemd=true

ciphers=(
aes-128-gcm
aes-256-gcm
chacha20-ietf-poly1305
)

_load() {
  local _dir="/etc/v2ray/233boy/v2ray/src/"
  . "${_dir}$@"
}

_sys_timezone() {
  IS_OPENVZ=
  if hostnamectl status | grep -q openvz; then
    IS_OPENVZ=1
  fi
  timedatectl set-timezone Asia/Shanghai
  timedatectl set-ntp true
}

_sys_time() {
  timedatectl status | sed -n '1p;4p'
  [[ $IS_OPENV ]]
}

v2ray_config() {
  v2ray_transport=$V2RAY_TRANSPORT
  tls_config
}

tls_config() {
  local random=$(shuf -i20001-65535 -n1)
  [ -z "$v2ray_port" ] && v2ray_port=$random
  domain="$DOMAIN"
  get_ip
  record="Y"
  domain_check
  if [[ $v2ray_transport -eq 4 ]]; then
    auto_tls_config
  else
    caddy=true
    install_caddy_info="打开"
  fi

  if [[ $caddy ]]; then
    path_config
  fi
}

auto_tls_config() {
  auto_install_caddy="Y"
  caddy=true
  install_caddy_info="打开"
}

path_config() {
  path_ask="Y"
  path="2omSSMquenched"
  [[ -z $path ]] && path="233blog"
  is_path=true
  proxy_site_config
}
proxy_site_config() {
  proxy_site="https://liyafly.com"
}

blocked_hosts() {
  blocked_ad=""
  [[ -z $blocked_ad ]] && blocked_ad="n"
  case $blocked_ad in
    Y | y)
      blocked_ad_info="开启"
      ban_ad=true
      ;;
    N | n)
      blocked_ad_info="关闭"
      ;;
  esac
}

domain_check() {
  test_domain=$(ping $domain -c 1 -W 2 | head -1)
  if [[ ! $(echo $test_domain | grep $ip) ]]; then
    exit 1
  fi
}

install_caddy() {
  # download caddy file then install
  _load download-caddy.sh
  _download_caddy_file
  _install_caddy_service
  _load caddy-config.sh
  # systemctl restart caddy
  do_service restart caddy

}

install_v2ray() {
  $cmd update -y
  if [[ $cmd == "apt-get" ]]; then
    $cmd install -y lrzsz git zip unzip curl wget qrencode libcap2-bin dbus
  else
    # $cmd install -y lrzsz git zip unzip curl wget qrencode libcap iptables-services
    $cmd install -y lrzsz git zip unzip curl wget qrencode libcap
  fi
  ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
  [ -d /etc/v2ray ] && rm -rf /etc/v2ray
  # date -s "$(curl -sI g.cn | grep Date | cut -d' ' -f3-6)Z"
  _sys_timezone
  _sys_time

  if [[ $local_install ]]; then
    if [[ ! -d $(pwd)/config ]]; then
      exit 1
    fi
    mkdir -p /etc/v2ray/233boy/v2ray
    cp -rf $(pwd)/* /etc/v2ray/233boy/v2ray
  else
    pushd /tmp
    git clone https://github.com/233boy/v2ray -b "$_gitbranch" /etc/v2ray/233boy/v2ray --depth=1
    # Changing the URL name
    sed -i "s/233v2.com_\${domain}/ZanZendegiAzadi-$URL_NAME/g" /etc/v2ray/233boy/v2ray/v2ray.sh
    sed -i "s/233v2.com_\${ip}/ZanZendegiAzadi-$URL_NAME/g"     /etc/v2ray/233boy/v2ray/v2ray.sh
    sed -i "s/233v2_\${domain}/ZanZendegiAzadi-$URL_NAME/g"     /etc/v2ray/233boy/v2ray/v2ray.sh
    popd
  fi

  if [[ ! -d /etc/v2ray/233boy/v2ray ]]; then
    exit 1
  fi

  # download v2ray file then install
  _load download-v2ray.sh
  _download_v2ray_file
  _install_v2ray_service
  _mkdir_dir
}

config() {
  cp -f /etc/v2ray/233boy/v2ray/config/backup.conf $backup
  cp -f /etc/v2ray/233boy/v2ray/v2ray.sh $_v2ray_sh
  chmod +x $_v2ray_sh

  v2ray_id=$uuid
  alterId=0
  ban_bt=true
  if [[ $v2ray_transport -ge 18 && $v2ray_transport -ne 33 ]]; then
    v2ray_dynamicPort_start=${v2ray_dynamic_port_start_input}
    v2ray_dynamicPort_end=${v2ray_dynamic_port_end_input}
  fi
  _load config.sh

  # systemctl restart v2ray
  do_service restart v2ray
  backup_config

}

backup_config() {
  sed -i "18s/=1/=$v2ray_transport/; 21s/=2333/=$v2ray_port/; 24s/=$old_id/=$uuid/" $backup
  if [[ $v2ray_transport -ge 18 && $v2ray_transport -ne 33 ]]; then
    sed -i "30s/=10000/=$v2ray_dynamic_port_start_input/; 33s/=20000/=$v2ray_dynamic_port_end_input/" $backup
  fi
  if [[ $shadowsocks ]]; then
    sed -i "42s/=/=true/; 45s/=6666/=$ssport/; 48s/=233blog.com/=$sspass/; 51s/=chacha20-ietf/=$ssciphers/" $backup
  fi
  [[ $v2ray_transport == [45] || $v2ray_transport == 33 ]] && sed -i "36s/=233blog.com/=$domain/" $backup
  [[ $caddy ]] && sed -i "39s/=/=true/" $backup
  [[ $ban_ad ]] && sed -i "54s/=/=true/" $backup
  if [[ $is_path ]]; then
    sed -i "57s/=/=true/; 60s/=233blog/=$path/" $backup
    sed -i "63s#=https://liyafly.com#=$proxy_site#" $backup
  fi
}

get_ip() {
  export "$(wget -4 -qO- https://dash.cloudflare.com/cdn-cgi/trace | grep ip=)" >/dev/null 2>&1
  [[ -z $ip ]] && export "$(wget -6 -qO- https://dash.cloudflare.com/cdn-cgi/trace | grep ip=)" >/dev/null 2>&1
  [[ -z $ip ]] && echo -e "\n$red 获取IP失败, 这垃圾小鸡扔了吧！$none\n" && exit
}

do_service() {
  if [[ $systemd ]]; then
    systemctl $1 $2
  else
    service $2 $1
  fi
}

install() {
  if [[ -f /usr/bin/v2ray/v2ray && -f /etc/v2ray/config.json ]] && [[ -f $backup && -d /etc/v2ray/233boy/v2ray ]]; then
    echo "V2ray is already installed. Here is the URL:"
    v2ray url | grep "vmess://\|vless://"
    exit 1
  elif [[ -f /usr/bin/v2ray/v2ray && -f /etc/v2ray/config.json ]] && [[ -f /etc/v2ray/233blog_v2ray_backup.txt && -d /etc/v2ray/233boy/v2ray ]]; then
    exit 1
  fi
  v2ray_config
  blocked_hosts
  install_v2ray
  if [[ $caddy || $v2ray_port == "80" ]]; then
    if [[ $cmd == "yum" ]]; then
      [[ $(pgrep "httpd") ]] && systemctl stop httpd
      [[ $(command -v httpd) ]] && yum remove httpd -y
    else
      [[ $(pgrep "apache2") ]] && service apache2 stop
      [[ $(command -v apache2) ]] && apt-get remove apache2* -y
    fi
  fi
  [[ $caddy ]] && install_caddy

  get_ip
  config
}

uninstall() {
  [[ -d /etc/v2ray ]] && lets_uninstall=true
  [[ -z $lets_uninstall ]] && echo "v2ray is already uninstalled." && return
  v2ray_pid=$(pgrep -f /usr/bin/v2ray/v2ray)
  caddy_pid=$(pgrep -f /usr/local/bin/caddy)
  [[ -f /etc/network/if-pre-up.d/iptables ]] && rm -rf /etc/network/if-pre-up.d/iptables
  [ $v2ray_pid ] && do_service stop v2ray
  rm -rf /usr/bin/v2ray
  rm -rf $_v2ray_sh
  sed -i '/alias v2ray=/d' /root/.bashrc
  rm -rf /etc/v2ray
  rm -rf /var/log/v2ray
  [ $caddy_pid ] && do_service stop caddy
  rm -rf /usr/local/bin/caddy
  rm -rf /etc/caddy
  rm -rf /etc/ssl/caddy

  if [[ $systemd ]]; then
    systemctl disable v2ray >/dev/null 2>&1
    rm -rf /lib/systemd/system/v2ray.service
    systemctl disable caddy >/dev/null 2>&1
    rm -rf /lib/systemd/system/caddy.service
  else
    update-rc.d -f caddy remove >/dev/null 2>&1
    update-rc.d -f v2ray remove >/dev/null 2>&1
    rm -rf /etc/init.d/caddy
    rm -rf /etc/init.d/v2ray
  fi
}


get_url() {
  url=$(v2ray url | grep "vmess://\|vless://" | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g")
  [[ $? != 0 ]] && echo "v2ray is not installed or you are not root." && exit 0
  config_b64=$(echo -n "$url" | cut -f 3 -d '/')
  config=$(echo $config_b64 | base64 --decode)
  new_port=$(head -1 /etc/caddy/Caddyfile | cut -f 2 -d ":" | cut -f 1 -d " ")
  is_number='^[0-9]+$'
  ! [[ $new_port =~ $is_number ]] && new_port=443
  config=$(echo -n $config | sed "s/\<443\>/$new_port/g")
  config_b64=$(echo "$config" | base64 -w 0)
  url="vmess://$config_b64"
  echo $url
  exit 0
}

_gitbranch="master"
DOMAIN=$1
V2RAY_TRANSPORT=$2
URL_NAME=$3
IP=$4
SSH_PORT=$5
REINSTALL=$6

[[ $1 == 'uninstall' ]] && uninstall && echo "v2ray is uninstalled seccussfully." && exit 0
[[ $1 == 'url' ]] && get_url exit 0
[[ -z $V2RAY_TRANSPORT ]] && V2RAY_TRANSPORT=4 # 4:WebSocket + TLS, 33:VLESS_WebSocket_TLS
[[ -z $URL_NAME ]] && URL_NAME="MahsaAmini"
[[ $V2RAY_TRANSPORT != 4 && $V2RAY_TRANSPORT != 33 ]] && echo "Your second argument must be 4 or 33." && exit 1

[[ -n $REINSTALL ]] && uninstall
install

echo "Finished seccussfully. In few seconds the v2ray config will be printed...."
sleep 2

