# Server Setup Script

This script is used for setting up a server with options to change the SSH port, create a VPN, change the server's password, enable swap space, update repositories and install hany tools.

## Usage

To run the script, use the following command:
```
bash prepare.sh -r [true|false] -p SSH_PORT -d DOMAIN -n VPN_NAME -k PROTOCOL[vmess|vless] -P CADDY_PORT -c -s -R -U -L -h
```

## Options

| Option | Description | Default |
| --- | --- | --- |
| `-r` | reboot at the end | true |
| `-p` | SSH Port number to use | false |
| `-d` | Domain name to use | false |
| `-n` | VPN name to be used in the URL | IRAN-RANDOM |
| `-k` | The protocol used for the VPN creation i.e. vmess or vless | false |
| `-P` | Changes the caddy port to the new one | false |
| `-c` | Enable password change |  |
| `-s` | Enable swap space |  |
| `-R` | Reinstall the VPN |  |
| `-U` | Uninstall the VPN |  |
| `-L` | Query the VPN URL |  |
| `-h` | Show the help message |  |

### Note

- This script is designed to work on Ubuntu systems.

- If DOMAIN, and PROTOCOL arguments are provided at the same time, the script will create a VPN. If none of them are provided, it will not create a VPN and instead, perform server setup actions like changing SSH port, updating repositories and so on.
Please make sure you understand the options before executing this script, as it will make changes to your system and may cause unexpected behavior if used improperly.

## Examples

1. To run the script with default options:

```
bash prepare.sh
```

2. To run the script with a different SSH port and without rebooting:
```
bash prepare.sh -r false -p 2222
```

3. To run the script with a different SSH port and create a VPN:
```
bash prepare.sh -p 2222 -d example.com -n myvpn -k vmess
```

4. To show the help message:
```
bash prepare.sh -h
```
