#!/bin/bash


set_caddy_port() {
  CADDY_PORT=$1
  if [[ -n $CADDY_PORT ]]; then
    is_number='^[0-9]+$'
    ! [[ $CADDY_PORT =~ $is_number ]] && echo "Error: Caddy port must be a number: No port is set for Caddy" && return
    # Change the CADDY port
    domain_caddy="$(cat /etc/caddy/Caddyfile | head -1 | awk '{ print $1 }')"
    sed -i "s/${domain_caddy}/${domain_caddy}:${CADDY_PORT}/g" /etc/caddy/Caddyfile
    sed -i "s/\<443\>/${CADDY_PORT}/g" /etc/v2ray/233blog_v2ray_config.json
    systemctl stop caddy
    systemctl stop v2ray
    systemctl start caddy
    systemctl start v2ray
  fi
}

# Default values
URL_NAME=IRAN-${RANDOM}
CHPASS=false
SWAP=false
REBOOT=true

while getopts "r:p:d:n:k:P:csURLh" opt; do
  case $opt in
    r) REBOOT=${OPTARG,,}
      if [[ $REBOOT != "true" && $REBOOT != "false" ]]; then
        echo "Error: Invalid value for option -r, allowed values are true or false"
        exit 1
      fi
    ;;
    p) SSH_PORT=${OPTARG,,}
    ;;
    P) CADDY_PORT=${OPTARG}
    ;;
    d) DOMAIN=${OPTARG,,}
    ;;
    n) URL_NAME=$OPTARG
    ;;
    k) PROTOCOL=${OPTARG,,}
      if [[ $PROTOCOL != "vmess"  && $PROTOCOL != "vless" ]]; then
        echo "Error: Invalid value for option -k, allowed values are vmess or vless"
        exit 1
      fi
      [[ $PROTOCOL == "vmess" ]] && PROTOCOL=4
      [[ $PROTOCOL == "vless" ]] && PROTOCOL=33
    ;;
    c) CHPASS=true
    ;;
    s) SWAP=true
    ;;
    U) VPN_UNINSTALL=true
    ;;
    R) VPN_REINSTALL=true
    ;;
    L) GET_VPN_URL=true
    ;;
    h) echo "Usage: prepare.sh -r [true|false] -p SSH_PORT -d DOMAIN -n VPN_NAME -k PROTOCOL[vmess|vless] -P CADDY_PORT -c -s -R -U -L -h"
      echo "Options:"
      echo "  -r  reboot at the end (default: true)"
      echo "  -p  Port number to use (default: false)"
      echo "  -d  Domain name to use (default: false)"
      echo "  -n  VPN name to be used in the URL (default: IRAN-RANDOM)"
      echo "  -k  The protocol used for the VPN creation i.e. vmess|vless. No protocol then no VPN. (default: false)"
      echo "  -P  Changes the caddy port to the new one (default: false)"
      echo "  -c  Enable password change"
      echo "  -s  Enable swap space"
      echo "  -U  Uninstall the VPN"
      echo "  -R  Reinstall the VPN"
      echo "  -L  Get the URL of the VPN"
      echo "  -h  Show this help message"
      exit 0
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
      exit 1
    ;;
  esac
done

IP=$(hostname -I | awk '{ print $1 }')

if [[ -n $GET_VPN_URL ]]; then
  bash v2ray.sh url
  exit 0
fi

if [[ -n $VPN_UNINSTALL ]]; then
  echo "VPN is being uninstalled..."
  bash v2ray.sh uninstall
  echo VPN is uninstalled successfully.
  exit 0
fi

[[ -n $CADDY_PORT ]] && set_caddy_port $CADDY_PORT

if [[ -n $VPN_REINSTALL ]]; then
  echo "VPN is being reinstalled..."
  echo "Therefore server-setup actions e.g. SSH port, system upgrade and etc are droped."
  sleep 3
  [[ -z $DOMAIN ]] && DOMAIN=$(grep "$IP" /etc/hosts | awk '{ print $2 }')
  [[ -z $PROTOCOL ]] && PROTOCOL=4 && v2ray url | grep -q 'vless' && PROTOCOL=33;
  SSH_PORT=$(cat /etc/ssh/sshd_config | grep ^Port | head -1 | awk '{ print $2 }')
  [[ -z $SSH_PORT ]] && SSH_PORT=22
  #[[ $URL_NAME == 'IRAN' ]] && URL_NAME=$(find /etc/v2ray/ -type f | xargs -I{} grep -o -E "ZanZendegiAzadi-.*"  {}) && URL_NAME=${URL_NAME::-2}
  sed -i "/^127.0.1.1/s/^/#/" /etc/hosts
  sed -i "/^$IP/s/^/#/" /etc/hosts
  echo "${IP} ${DOMAIN}" >> /etc/hosts
  bash v2ray.sh $DOMAIN $PROTOCOL $URL_NAME $IP $SSH_PORT $VPN_REINSTALL
  set_caddy_port $CADDY_PORT
  echo VPN is created successfully.
  exit 0
fi

[[ -z $PROTOCOL && -z $DOMAIN ]] && create_vpn=false
[[ -n $PROTOCOL && -n $DOMAIN ]] && create_vpn=true
[[ -z create_vpn ]] && echo "PROTOCOL, DOMAIN arguments must be provided at the same time OR provide none of them." && exit 1

if [[ $create_vpn == "true" ]]; then
  echo "Creating VPN..."
  echo "Therefore server-setup actions e.g. SSH port, system upgrade and etc are droped."
  sleep 3
  sed -i "/^127.0.1.1/s/^/#/" /etc/hosts
  sed -i "/^$IP/s/^/#/" /etc/hosts
  echo "${IP} ${DOMAIN}" >> /etc/hosts
  bash v2ray.sh $DOMAIN $PROTOCOL $URL_NAME $IP $SSH_PORT
  set_caddy_port $CADDY_PORT
  echo VPN is created successfully.
  exit 0
fi

if [[ $CHPASS == 'true' ]]; then
  echo 'Changing password for ubuntu user...'
  sudo passwd ubuntu

  echo 'changing password for root superuser...'
  passwd
fi

echo 'Updating repositories and installing updates and needed tools...'
sleep 2
apt update
sudo DEBIAN_FRONTEND=noninteractive apt -y upgrade
sudo DEBIAN_FRONTEND=noninteractive apt -y install net-tools nload pip

is_number='^[0-9]+$'
if [[ $SSH_PORT =~ $is_number ]]; then
  echo 'Changing ssh port...'
  sleep 1
  sed "s/#Port\ 22/Port\ $SSH_PORT/g" -i /etc/ssh/sshd_config
fi

if [[ $SWAP == 'true' ]]; then
  echo "Creating and adding a swap file with 2000 MB in size."
  dd if=/dev/zero of=/swapfile bs=1M count=2000 && mkswap /swapfile && swapon /swapfile
fi

if [[ $REBOOT == 'true' ]]; then
  echo "The server is set up successfully."
  echo -n "Rebooting in 5 seconds..."
  for i in {5..1}; do
    echo -n "$i "
    sleep 1
    echo -en "\r"
  done
  reboot
else
  echo "Reboot option is set to false, skipping reboot..."
fi

